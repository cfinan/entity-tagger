# Getting Started with entity-tagger

__version__: `0.1.1a0`

The entity-tagger package is a toolkit to manually annotate data, either by adding named entity tags to strings, or labelling of data in some way. It is a web-app built using flask and sqlalachemy (not flask-sqlalachemy) and uses a Javascript based front end.

There is [online](https://cfinan.gitlab.io/entity-tagger/index.html) documentation for entity-tagger.

## Installation instructions
At present, entity-tagger is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that you install in either of the two ways listed below.

### Installation using conda
I maintain a conda package in my personal conda channel. To install from this please run:

```
conda install -c cfin -c biopython -c conda-forge entity-tagger
```

There are currently builds for Python v3.8, v3.9 and v3.10 for Linux-64 and Mac-osx. Please keep in mind that all development is carried out on Linux-64 and Python v3.8/v3.9. I do not own a Mac so can't test on one, the conda build does run some import tests but that is it.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/entity-tagger.git
cd entity-tagger
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the repository. The difference with this is that you can just to a `git pull` to update, or switch branches without re-installing:
```
python -m pip install -e .
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/envs` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.8, v3.9.
