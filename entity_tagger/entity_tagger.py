"""Run an entity tagging project.
"""
# Importing the version number (in __init__.py) and the package name
from entity_tagger import (
    __version__,
    __name__ as pkg_name,
    log,
    config,
    database,
    app,
    query
)
from tqdm import tqdm
import argparse
import sys
import os
import pprint as pp


_SCRIPT_NAME = "entity-tagger"
"""The name of the script (`str`)
"""
# Use the module docstring for argparse
_DESC = __doc__
"""The program description (`str`)
"""
_LOCAL_TABLE_CLASS = 'LocalTable'
"""The local table class name (`str`)
"""
_LOCAL_TABLE_NAME = 'local_table'
"""The local table name (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script. For API use see
    ``something else``.
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(
        _SCRIPT_NAME, verbose=args.verbose
    )
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    conf = config.ConfigFile(args.config_file)
    try:
        run_entity_tagger(conf, skip_file_check=args.skip,
                          verbose=args.verbose)
    except (BrokenPipeError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)
        os.dup2(devnull, sys.stdout.fileno())
        log.log_interrupt(logger)
    finally:
        log.log_end(logger)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added
    """
    parser = argparse.ArgumentParser(
        description=_DESC
    )

    parser.add_argument('config_file',
                        type=str,
                        help="The project config file")
    parser.add_argument('-s', '--skip', action="store_true",
                        help="Do not recheck the the file MD5 before starting")
    parser.add_argument('-v', '--verbose',  action="store_true",
                        help="give more output")

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Parse the command line arguments.

    Parameters
    ----------
    parser : :obj:`argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object containing the arguments
    """
    args = parser.parse_args()
    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def run_entity_tagger(config_file, verbose=False, skip_file_check=False):
    """The API entry point for entity-tagging based on config file options.

    Parameters
    ----------
    config : `entity_tagger.config.Config`
        An entity tagging config obejct.
    verbose : `bool`, optional, default: `False`
        Issue progress and log updates.
    skip_file_check : `bool`, optional, default: `False`
        Once initialise do not recheck the input file for changes. This
        currently has no effect.
    """
    # The call to getLogger will return the same logger object if the name is
    # the same
    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=verbose)

    check = check_initialised(config_file)
    init_config = None
    # check = False
    if check is False:
        logger.info("project not initialised")
        init_config = initialise(config_file, verbose=verbose)
    elif check is True and skip_file_check is True:
        logger.info("project initialised, checking file...")
        init_config = config.ConfigFile(config_file.init_file)
    else:
        logger.info("project initialised, skipping file file check...")
        init_config = config.ConfigFile(config_file.init_file)

    LocalTable = database.get_table(
        _LOCAL_TABLE_CLASS, _LOCAL_TABLE_NAME,
        [i.sqlalchemy for i in init_config.columns]
    )
    print(LocalTable)
    session_handler = database.SessionHandler(
        (init_config.data_url, 'data')
    )
    query_int = query.QueryInterface(session_handler, LocalTable)
    print(query_int.count)
    etapp = app.create_app(
        init_config.name, query_int, init_config.labels, init_config.columns,
        row_annotate=init_config.row_annotate,
        col_annotate=init_config.col_annotate,
        text_annotate=init_config.text_annotate
    )
    etapp.run(debug=True)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def check_initialised(config_file):
    """Check to see if the project has been initialised.

    Parameters
    ----------
    config_file : `entity_tagger.config.Config`
        An entity tagging config object.

    Notes
    -----
    This checks for the presence of a file called ``.init.cfg`` in the root of
    the project directory.
    """
    try:
        open(config_file.init_file).close()
        return True
    except FileNotFoundError:
        return False


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def initialise(config_file, verbose=False):
    """Initialise a file for tagging.

    Parameters
    ----------
    config_file : `entity_tagger.config.Config`
        An entity tagging config object.
    verbose : `bool`, optional, default: `False`
        Report initialisation progress.
    """
    if config_file.infile is None:
        raise ValueError("no input file section is defined in the config file")

    logger = log.retrieve_logger(_SCRIPT_NAME, verbose=verbose)

    logger.info("checking file MD5 hash...")
    md5hash = config_file.infile.get_md5()
    logger.info(f"MD5 hash: {md5hash}")
    # print(config_file)

    # There may be columns supplied in the config file, if so we use those to
    # load the data
    if len(config_file.columns) == 0:
        logger.info("determine data types...")
        # Determine the data types in the file.
        dtypes = config_file.infile.get_dtypes(verbose=verbose)
        for i in dtypes:
            config_file.add_column(i)
    else:
        dtypes = config_file.columns

    logger.info(f"there are {config_file.infile.nrows} rows")
    # print(config_file)
    # Dynamically create an ORM table class
    LocalTable = database.get_table(
        _LOCAL_TABLE_CLASS, _LOCAL_TABLE_NAME, [i.sqlalchemy for i in dtypes]
    )
    # engine = database.get_engine(config_file.data_url)
    # print(type(engine))
    # print(LocalTable)
    load_data(config_file, LocalTable, dtypes, verbose=verbose)
    config_file.write_config(config_file.init_file)
    return config_file


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def load_data(config_file, table, dtypes, verbose=False):
    """Initialise a file for tagging.

    Parameters
    ----------
    config_file : `entity_tagger.config.Config`
        An entity tagging config object.
    table : `LocalTable`
        A dynamically created ``LocalTable`` orm class to load the data.
    dytpes : `list` of `entity_tagger.columns.Column`
        The column definitions that were used to built the ``LocalTable``. This
        may be a subset of the columns in the file.
    verbose : `bool`, optional, default: `False`
        Report initialisation progress.
    """
    engine = database.get_engine(config_file.data_url)
    database.Base.metadata.create_all(bind=engine)
    sessionmaker = database.get_local_sessionmaker(engine)
    session = sessionmaker()

    tqdm_kwargs = dict(
        desc="[info] loading rows",
        unit=" rows",
        disable=not verbose,
        total=config_file.infile.nrows
    )
    reader = config_file.infile.read()
    header = next(reader)

    for row in tqdm(reader, **tqdm_kwargs):
        # Map the header into a dict of columns
        row = dict([(i, row[idx]) for idx, i in enumerate(header)])
        load_row = {}
        # Use the column definitions that we want to load to extract
        # the specific data to load
        for i in dtypes:
            try:
                load_row[i.name] = i.dtype(row[i.name])
            except ValueError as e:
                if i.nullable is True:
                    load_row[i.name] = None
                else:
                    raise ValueError(f"column {i.name}: {e}") from e
        session.add(table(**load_row))
    session.commit()


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
