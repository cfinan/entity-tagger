"""Constants for the entity_tagger
"""
import os

# #### CONFIG DEFAULTS ####
DEFAULT_DB_BACKEND = 'sqlite'
"""The SQLAlchemy backend for file based projects (`str`)
"""
DEFAULT_DRIVER = None
"""The default python database driver (`str`)
"""
DEFAULT_USER_DB_NAME = "users"
"""The default user database name (`str`)
"""

# #### LABEL CONSTANTS ####
LABEL_ADD_ROW = 1 << 0
"""Is a label able to be added to a row (int)
"""
LABEL_ADD_CELL = 1 << 1
"""Is a label able to be added to a cell (int)
"""
LABEL_ADD_ENTITY = 1 << 2
"""Is a label able to be added to an entity (a component of a row) (int)
"""
DEFAULT_LABEL_COLOURS = [
    "red", "blue", "green", "orange"
]
"""The default colour cycling for the labels (`list` of `str`)
"""
