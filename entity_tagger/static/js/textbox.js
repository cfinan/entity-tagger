/*
  classes for handling the highlighting and label type of the text in a
   text box <p> tag
*/


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SelectText {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    constructor(elementId, currentText) {
        this.nodeId = elementId;
        this.currentText = currentText;
        this.currentSelections = {};
        this.stagedSelections =[];
        this.getNode();
        this.setText(this.currentText);
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    getNode() {
        this.node = document.querySelectorAll("p#"+this.nodeId);
        if (this.node.length != 1) {
            alert(
                "can't find textbox, there should be a " +
                    "unique <p> textbox in HTML"
            );
            // TODO: How to stop execution at this point??
        }
        this.node = this.node[0];
        // console.log(`Price of ${this.name} is ${this.price}`);
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    setText(newText) {
        this.currentText = newText;
        this.node.textContent = newText;
        const prevSelections = this.currentSelections = {};
        this.currentSelections = {};
        return prevSelections;
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    addNewSelection(region) {
        if (region.id() in this.currentSelections) {
            
            return false;
        }
        this.currentSelections[region.id()] = region;
        return true;
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    #addSelection(region) {
        
    }
}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SelectedRegion {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    constructor(startIndex, endIndex, selectedString, classNames) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.selectedString = selectedString;
        if (this.classNames == undefined) {
            this.classNames = new Set();
        }
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    length() {
        return this.endIndex - this.startIndex;
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    id() {
        return `${this.startIndex}-${this.endIndex}`;
    }

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    copy() {
        return new SelectedRegion(
            this.startIndex, this.EndIndex, this.selectedString
        );
    }

}
// const pen1 = new Pen("Marker", "Blue", "$3");
// pen1.showPrice();

