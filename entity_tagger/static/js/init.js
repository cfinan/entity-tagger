$(document).ready(setup);

// The ASCII value for enter
const ENTER_KEY = 13;
const DATA_TABLE_ID = "data";
const DATA_TABLE_ID_SEARCH = "#" + DATA_TABLE_ID;
const START_TAG = "<< ";
const END_TAG = " >>";
const DELIMITER = " | ";


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
const moveScrollX = function() {
    /* Helper to move the scrollX scroll bar in datatables to below the footer
     */

    // see: https://stackoverflow.com/questions/30843811
    // Disable TBODY scoll bars
    $('.dataTables_scrollBody').css({
        'overflow': 'hidden',
        'border': '0'
    });

    // Enable TFOOT scoll bars
    $('.dataTables_scrollFoot').css('overflow', 'auto');

    // Sync TFOOT scrolling with TBODY
    $('.dataTables_scrollFoot').on('scroll', function () {
        $('.dataTables_scrollBody').scrollLeft($(this).scrollLeft());
    });
};


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function setup() {
    /* Initialise all the components on the page
     */
    console.log("initialising");
    console.log(INIT_ROOT);

    // The URL to get the table column data from the server
    let setupUrl = INIT_ROOT + "/setup";

    // Fetch and resolve to create the datatable
    // TODO: Handle any error states
    fetch(setupUrl)
        .then(response => response.json())
        .then(setupData => initialise(setupData));
};


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function initialise(setupData) {
    /* Initialise all the components on the page
     */
    let textBox = new SelectText('current-cell');
    const table = createDataTable(setupData, textBox);
    textHighlightEvents(table, textBox);
};


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function textHighlightEvents(table, textBox) {
    /* Setup the events on the textBox that are fired when the user selects
       something */
    textBox.node.addEventListener('mouseup', function(e) {
        // [startIndex, endIndex, selectedString]
        const selection = getSelectedRegion(textBox.node);
        // If the selection has some length
        if (selection[0] != selection[1]) {
            const curCell = table.cell({selected: true});
            const curRow = table.row([curCell[0][0].row]);
            const region = new SelectedRegion(
                selection[0], selection[1], selection[2]
            );
            console.log(region);
            console.log(region.id());
            isAdded = textBox.addNewSelection(region);
            console.log(isAdded);
            // const selections = embedSelection(
            //     curCell,
            //     curRow.data().ATTRIBUTES,
            //     selection[0],
            //     selection[1],
            //     selection[2]
            // );
            // renderMarks(selections);
        };
    });
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function getSelectedRegion(container) {
    /* Get the positions and values of the text selected by the user */
    const s = window.getSelection();
    const range = s.getRangeAt(0);
    let selectionStart = range.startOffset;
    let selectionEnd = range.endOffset;
    let selectedString = s.toString();

    const charsBeforeStart = getCharactersCountUntilNode(
        range.startContainer, container
    );
    const charsBeforeEnd = getCharactersCountUntilNode(
        range.endContainer, container
    );
    if (charsBeforeStart < 0 || charsBeforeEnd < 0) {
        console.warn('out of range');
        return [-1, -1, ""];
    }
    let startIndex = charsBeforeStart + selectionStart;
    let endIndex = charsBeforeEnd + selectionEnd;

    return [startIndex, endIndex, selectedString];
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function getCharactersCountUntilNode(node, parent) {
    var walker = document.createTreeWalker(
        parent || document.body,
        NodeFilter.SHOW_TEXT,
        null,
        false
    );
    var found = false;
    var chars = 0;
    while (walker.nextNode()) {
        if(walker.currentNode === node) {
            found = true;
            break;
        }
        chars += walker.currentNode.textContent.length;
    }
    if(found) {
        return chars;
    }
    else return -1;
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function getDisplaySelect(selection) {
    return {
        selectStart: selection.selectStart,
        selectEnd: selection.selectEnd,
        realStart: selection.realStart,
        realEnd: selection.realEnd,
        length: selection.length,
        selString: selection.selString,
        className: undefined
    };
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function renderMarks(selections) {
    var context = document.querySelector("#current-cell");
    var instance = new Mark(context);
    instance.unmark();

    selections.sort(sortShortest);

    const displaySelections = [];
    const textBlocks = [];
    let has_ol = false;
    for (let i = 0; i < selections.length; i++) {
        for (let j = 0; j < textBlocks.length; j++) {
            const ol = overlap(textBlocks[j], selections[i]);
            console.log("*** OVERLAP ***");
            console.log(ol);
            if (ol > 0) {
                has_ol = true;

                let leftOverhang = (
                    textBlocks[j].realStart - selections[i].realStart
                );
                let rightOverhang = (
                    selections[i].realEnd - textBlocks[j].realEnd
                );

                console.log("leftOverhang");
                console.log(leftOverhang);
                console.log("rightOverhang");
                console.log(rightOverhang);
                console.log("**** TEXT BLOCKS *****");
                console.log(textBlocks[j].realStart);
                console.log(textBlocks[j].realEnd);
                if (leftOverhang > 0) {
                    let leftFragment = getDisplaySelect(selections[i]);
                    leftFragment.selectEnd = textBlocks[j].realStart;
                    leftFragment.length = (
                        leftFragment.selectEnd - leftFragment.selectStart
                    );
                    displaySelections.push(leftFragment);
                    textBlocks[j].realStart -= leftOverhang;
                }
                if (rightOverhang > 0) {
                    let rightFragment = getDisplaySelect(selections[i]);
                    rightFragment.selectStart = textBlocks[j].realEnd;
                    rightFragment.length = (
                        rightFragment.selectEnd - rightFragment.selectStart
                    );
                    displaySelections.push(rightFragment);
                    textBlocks[j].realEnd += rightOverhang;
                }
            }
        }
        if (has_ol === false) {
            textBlocks.push(
                {
                    realStart: selections[i].realStart,
                    realEnd: selections[i].realEnd
                }
            );
            displaySelections.push(selections[i]);
        }
    }

    console.log("**** DISPLAY THESE ****");
    console.log(displaySelections);
    console.log("**** BLOCKS ****");
    console.log(textBlocks);
    console.log(textBlocks.length);
    for (i in displaySelections) {
        let className = displaySelections[i].className;
        if (className === undefined) {
            className = "unassigned";
        }
        const markThis = [{
            start: displaySelections[i].selectStart,
            length: displaySelections[i].length
        }];
        instance.markRanges(markThis, {className: className});
    }
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function embedSelection(curCell, rowAttr, startIndex, endIndex,
                        selectedString) {
    const columnNo = curCell[0][0].column;
    // console.log(curCell);
    if (rowAttr.TEXT === undefined) {
        rowAttr.TEXT = {};
    }

    if (rowAttr.TEXT[columnNo] === undefined) {
        rowAttr.TEXT[columnNo] = [];
    }

    rowAttr.TEXT[columnNo].push(
        {
            selectStart: startIndex,
            selectEnd: endIndex,
            realStart: startIndex,
            realEnd: endIndex,
            length: endIndex - startIndex,
            selString: selectedString,
            className: undefined
        }
    );
    return rowAttr.TEXT[columnNo];
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function overlap(a, b) {
    return Math.max(
        0,
        Math.min(a.realEnd, b.realEnd) -
            Math.max(a.realStart, b.realStart)
    );
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function sortLeftmost(a, b) {
    if (a.realStart == b.realEnd) {
        return b.length - a.length;
    }
    return a.start - b.start;
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function sortShortest(a, b) {
    return a.length - b.length;
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function createDataTable(setupData, textBox) {
    /* Create the data table from the column data and setup events etc
       according to the column definitions
    */
    // Process the received columns into column objects for datatables
    let procColumns = processColumns(setupData.columns);

    // Use the column objects to create table footer search boxes
    setTableFooter(procColumns);

    // Initialise the datatable
    const table = $(DATA_TABLE_ID_SEARCH).DataTable({
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: {url: INIT_ROOT + "/data", type: 'GET'},
        columns: procColumns,
        fnInitComplete: moveScrollX,
        // Single and Cell select must be used for the event bindings to
        // work properly
        select: {style: 'single', items: 'cell'},
    });

    // Adjust the formatting of the footer and add search input events
    formatFooter(table);
    addFooterEvents(table);
    // Make sure the global search requires the ENTER key to be pressed to
    // search the server
    setGlobalSearchBindings();
    // Enable inline editing for columns tagged as editable
    setInlineEditEvents(table);
    // Set the table row/column selection based on the user preferences
    setTableSelectionEvents(
        table, textBox, setupData.row_annotate, setupData.col_annotate,
        setupData.text_annotate
    );
    return table;
};


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function setInlineEditEvents(table) {
    /* This sets the ability to inline edit cells of the table if their
       className is set to editable
    */
    // Big credit to:
    // https://stackoverflow.com/questions/31856961
    // Set edit status when the user double clicks on a cell.
    $(DATA_TABLE_ID_SEARCH).on(
        'dblclick', 'td.editable',
        function (e) {
            // I'm a noob, don't know what this means
            e.preventDefault();
            // I think there is some delay on when the events trigger
            // so sometimes the cell still contains the input element and
            // this check prevents accidentally creating another input element
            if (e.target.localName != 'input') {
                // This is a different way to get the row/column, not sure what
                // is best
                let row = e.target._DT_CellIndex.row;
                let col = e.target._DT_CellIndex.column;

                // We add a live edit class to the td being editted, this is
                // picked up by the onblur event
                $(table.cell(row, col).nodes()).addClass('live-edit');

                // Not 100% sure what this does
                if (!e.target.children.length) {
                    // create an input box in the td, here we fill with all
                    // the cell data as there may be a render function in place
                    e.target.innerHTML = '<input id="'+row+'-'+col+'"' +
                                         ' type="text" class="editor" value="' +
                        table.cell(row, col).data() + '">';
                    // Give the input focus, so the onblur will get fired
                    // when another event occurs
                    $("input#"+row+"-"+col).focus();
                }
            }
        }
    );

    // When a live edited td loses focus
    $(DATA_TABLE_ID_SEARCH).on(
        'blur', 'td.live-edit',
        function (e) {
            e.preventDefault();
            // I am not sure why but the target always seems to be the input
            // box not the td? So we check to make sure the parent is the
            // td before continuing
            if (e.target.parentNode.localName == 'td') {
                let row = e.target.parentNode._DT_CellIndex.row;
                let col = e.target.parentNode._DT_CellIndex.column;

                // Set the table cell data with the input box contents
                table.cell(row, col).data(e.target.value);

                // Remove the live edit class so will not be subject to the
                // event anymore
                $(table.cell(row, col).nodes()).removeClass('live-edit');
                // Finally remove the input box
                $("input#"+row+"-"+col).remove();
                // TODO Add an edit atomic update call to the server
            }
        }
    );
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function clearLabelchecks() {
    $('input.label-check').prop('checked', false);
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function setTableSelectionEvents(table, textBox, row_annotate, col_annotate,
                                 text_annotate) {
    /* Set the table row/column selection events based on the user preferences
       for what they want to annotate.
     */
    // default to row annotate only if all are false
    if (row_annotate == false && col_annotate == false && text_annotate == false) {
        row_annotate = true;
    };

    // If we are annotating rows only
    if (row_annotate == true && col_annotate == false) {
        // Everything is cell based so we can get the cell values. We fake the
        // row selection by setting the row class to selected
        $(DATA_TABLE_ID_SEARCH + ' tbody').on(
            'click', 'td',
            function(e) {
                // Remove any fake row selections
                $(table.rows().nodes()).removeClass('selected');
                // Get the current row index value (we are only doing single
                // selections)
                let row_idx = table.rows(this).indexes()[0];
                // Fake the row selections
                $(table.rows(row_idx).nodes()).addClass('selected');
                // TODO: Add a call to update the text display box when
                //  implemented
                // console.log(table.cell(this).data());
                clearLabelchecks();
            }
        );
    } else if (row_annotate == false && col_annotate == true) {
        // Only column annotation, then we do not have to do much as it is the
        // default
        $(DATA_TABLE_ID_SEARCH + ' tbody').on(
            'click', 'td',
            function(e) {
                // TODO: Add a call to update the text display box when
                //  implemented
                // console.log(table.cell(this).data());
                clearLabelchecks();
            }
        );
    } else if (row_annotate == true && col_annotate == true) {
        // Both row and column selection, we use our fake row selection again but
        // only if a CTRL key is pressed, otherwise we default to cell selection
        $(DATA_TABLE_ID_SEARCH + ' tbody').on(
            'click','td',
            function(e) {
                $(table.rows().nodes()).removeClass('selected');
                let row_only = false;
                if (e.ctrlKey) {
                    row_only = true;
                    let row_idx = table.rows(this).indexes()[0];
                    $(table.rows(row_idx).nodes()).addClass('selected');
                }
                // console.log(table.rows(this).data())
                // TODO: Add a call to update the text display box when
                //  implemented
                // console.log(table.cell(this).data());
                // $("p#current-cell").text(table.cell(this).data());
                textBox.setText(table.cell(this).data());
                clearLabelchecks();
            }
        );
    }
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function setGlobalSearchBindings() {
    /* This disables the search on keystroke and enables the search to happen
       after the user presses <ENTER>. This is to reduce server unwanted server
       queries
    */
    const table_filter_input = DATA_TABLE_ID_SEARCH + ' input';

    // Disable search on key up and implement enter
    $(table_filter_input).unbind();

    // Now bind to a specific search function
    $(table_filter_input).bind('keyup', function(e) {
        const table = $(DATA_TABLE_ID_SEARCH).DataTable();

        // If the current data in the search is different to what is entered
        // then update the table search. Note this will happen on each KeyPress
        // but not actually search until <ENTER> is pressed
        if (table.search() !== this.value) {
            table.search(this.value);
        }

        // The draw event on <ENTER> will send a request to the server
        if (e.keyCode == ENTER_KEY ) {
            table.draw();
        };
    });
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function addFooterEvents(table) {
    /* Add the search event handling to the footer input text boxes. This are
       set to fire when the ser presses enter. Events are lso added to the
       checkboxes that indicate that the user wants to use a non-exact LIKE
       query basically "%<serach_term%"
    */

    // I think this applies to the function to every column object in the table
    table.columns().every(function() {
        const col = this;

        // Get all the input boxes of the class table-search. These are the
        // boxes in the footer of the data table
        $('input.table-search', col.footer()).on(
            'keyup change',
            function(e) {
                // "this" is the input box, we only adjust the column search
                // API if the search term is different from what is currently
                // there
                if (col.search() !== this.value) {
                    col.search(this.value);
                };
                // if serverside draw() only on enter
                if (e.keyCode == ENTER_KEY ) {
                    col.draw();
                };
            });

        // Add events to the check boxes as well. These are of the class
        // exact-search
        $('input.exact-search', this.footer()).change(
            function(e) {
                // Use what is in the search already and just adjust the regex
                // argument
                col.search(col.search(), regex=this.checked);
            });
    });
}


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function setTableFooter(procColumns) {
    /* Build a DataTables footer based on the columns that we know will be in
       the datatable. This requires that a <tfoot> element has been set in the
       HTML.

       The footer will also consist of search boxes.
    */
    // Add the footer cells (th elements)
    for (let i in procColumns) {
        $(DATA_TABLE_ID_SEARCH + " tfoot tr").append(
            '<th\>'+procColumns[i].title+'</th\>'
        );
    };

    let idx = 0;

    // Setup - add a text input to each footer cell
    $(DATA_TABLE_ID_SEARCH + " tfoot tr th").each(
        function () {
            // We will honour the column searchable parameter, if it is True
            // the input box and check box will be set to disabled
            let disabled = "";
            if (procColumns[idx].searchable == false) {
                disabled = "disabled";
            };

            // This is the th element
            const title = $(this).text();
            // Some left/right padding for the th element
            $(this).addClass("px-1");

            // Within the th element add some HTML for the input box and a
            // tick box. This seems kkind of nasty but I have no idea what
            // best practice it (yet)
            $(this).html(
                '<div class="p-3">' +
                    '<div class="p-1 rounded bg-light text-dark">' +
                    '<input style="border: 0" ' +
                    'class="table-search pr-2 bg-light text-dark" ' +
                    'type="text" placeholder="Search ' +
                    title +
                    '" ' + disabled +
                    '/><input class="exact-search" ' +
                    'title="checkbox for LIKE searches" type="checkbox" ' +
                    'class="p-5" ' + disabled + '></div></div>'
            );
            idx = idx + 1;
        }
    );
};


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function formatFooter(table) {
    /* Format the data table footer to remove border classes and add some
       padding classes from bootstrap. There is probably a better way to do
       this.
    */
    // Remove all the footer borders
    $( table.table().footer() ).parent().removeClass(
        'table-bordered table-striped table'
    );
    // Add some top and bottom padding to the footer of the table
    $( table.table().footer() ).parent().addClass(
        'pt-2 pb-2'
    );
};


// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
function processColumns(columnData) {
    /*Process the column JSON into column objects for the data table
     */
    const procColumns = [];
    // TODO: Sort on a display index
    for (let i in columnData) {
        let col = {
            "title": columnData[i].display_name,
            "data": columnData[i].name,
            "visible": columnData[i].visible,
            "searchable": columnData[i].searchable,
            "bSearchable": columnData[i].searchable,
            "orderable": columnData[i].orderable
        };

        // For the editable functionality we set a class name that is picked up
        // by an event handler on the cell (td)
        if (columnData[i].editable == true) {
            col.className = "editable";
        };

        if (columnData[i].display_len > 0) {
            col.render = function(data, type, row, meta) {
                if (typeof(data) == 'string' && data.length > 10) {
                    return data.substr(0, columnData[i].display_len) + "...";
                }
                // console.log(data);
                return data;
            };
        };
        procColumns.push(
            col
        );
    };
    return procColumns;
}


// $(function() {
// get_data = function(e) {
//       $.getJSON($SCRIPT_ROOT + '/search_umls', {
//         a: $('input[name="a"]').val(),
//       }, function(data) {
//           $('#result').text(data.total_count);
//           $('input[name=a]').focus().select();
// 	  assignData(data);
//       });

//     $('input[name=a]').focus();
// };
