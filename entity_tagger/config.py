"""Handle interaction with the config file.
"""
from entity_tagger import (
    constants as c,
    labels,
    columns,
    file_init
)
try:
    from sqlalchemy.engine.URL import create as URL
except ImportError:
    from sqlalchemy.engine.url import URL

from pathlib import Path
import configparser
import os
import re
# import pprint as pp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Config(object):
    """A configuration object.

    Parameters
    ----------
    project_dir : `str`
        The project directory, if it does not exist it will be created.
    all_labels : `list` or `entity_tagger.label.Label`
        Label objects.
    user_url : `str`
        The SQLAlchemy connection URL for the user authentication database.
    data_url : `str`
        The SQLAlchemy connection URL for the data database.
    infile : `entity_tagger.file_init.FileHandler`, optional, default: `NoneType`
        A file options object.
    columns : `list` of `entity_tagger.columns.Column`
        The columns that should be displayed.
    row_annotate : `bool, optional, default: `True`
        Do you want to enable row level annotations.
    col_annotate : `bool, optional, default: `True`
        Do you want to enable column level annotations.
    text_annotate : `bool, optional, default: `True`
        Do you want to enable text string annotations level annotations.
    """
    CONFIG_FILE_EXT = ['.ini', '.cfg', '.cnf']
    """The known extensions for a config file (`list` of `str`)
    """
    INIT_FILE = ".init.ini"
    """The base name of the initialised config file (`str`)
    """
    PROJECT_SECTION = "project"
    """The config section header name for the project (`str`)
    """
    LABEL_SECTION = "labels"
    """The config section header name for the basic label section
    (`str`)
    """
    COMPLEX_LABEL_SECTION = "label"
    """The config section header prefix for a complex label section
    (`str`)
    """
    COMPLEX_COLUMN_SECTION = "column"
    """The config section header prefix for a complex column section
    (`str`)
    """
    INFILE_SECTION = "infile"
    """The config section header name for the input file (`str`)
    """
    DATABASE_SECTION = "database"
    """The config section header name for the database (`str`)
    """
    _EXPECTED_SECTIONS = [PROJECT_SECTION]
    """Sections that must exists in the configuration file (`list` of `str`)
    """

    # The section keys
    NAME_KEY = "name"
    """The config section key for the project name (`str`)
    """
    PATH_KEY = "path"
    """The config section header name for the project path (`str`)
    """
    ROW_ANNOTATE_KEY = 'row_annotate'
    """The config section header name for the project row annotation key
    (`str`)
    """
    COL_ANNOTATE_KEY = 'column_annotate'
    """The config section header name for the project column annotation key
    (`str`)
    """
    TEXT_ANNOTATE_KEY = 'text_annotate'
    """The config section header name for the project text annotation key(`str`)
    """
    DELIMITER_KEY = "delimiter"
    """The config section header name for the project (`str`)
    """
    ENCODING_KEY = "encoding"
    """The config section header name for the project (`str`)
    """
    COMMENT_KEY = "comment"
    """The config section header name for the project (`str`)
    """
    SKIPLINES_KEY = "skiplines"
    """The config section header name for the project (`str`)
    """
    USERS_DB_KEY = "users"
    """The config section header name for the project (`str`)
    """
    DATA_DB_KEY = "data"
    """The config section header name for the project (`str`)
    """
    LABEL_COLOR_KEY = "colour"
    """The config section key for the label colour (`str`)
    """
    LABEL_FONT_COLOR_KEY = "font_colour"
    """The config section key for the label font colour (`str`)
    """
    LABEL_DESC_KEY = "desc"
    """The config section key for the label description (`str`)
    """

    # Keys with string values
    COLUMN_DISPLAY_KEY = "display_name"
    """The config section key for the column display name (`str`)
    """
    COLUMN_INTERNAL_DELIMITER_KEY = "internal_delimiter"
    """The config section key for the column internal delimiter value (`str`)
    """
    # Keys expecting integer values
    COLUMN_MAX_LEN_KEY = "max_len"
    """The config section key for the column max length (`str`)
    """
    COLUMN_DISPLAY_LEN_KEY = "display_len"
    """The config section key for the column display length (`str`)
    """
    # Special handling of this
    COLUMN_DTYPE_KEY = "dtype"
    """The config section key for the column data type (`str`)
    """
    # Keys expecting boolean values (expressed as integers)
    COLUMN_NULLABLE_KEY = "nullable"
    """The config section key for the is nullable attribute (`str`)
    """
    COLUMN_INDEX_KEY = "index"
    """The config section key for the column is index (`str`)
    """
    COLUMN_ORDERABLE_KEY = "orderable"
    """The config section key for the column is orderable (`str`)
    """
    COLUMN_SEARCHABLE_KEY = "searchable"
    """The config section key for the column is searchable (`str`)
    """
    COLUMN_VISIBLE_KEY = "visible"
    """The config section key for the column visibility (`str`)
    """
    COLUMN_EDITABLE_KEY = "editable"
    """The config section key for the column editability (`str`)
    """
    COLUMN_EXACT_QUERY_KEY = "editable"
    """The config section key for the column exact querying (`str`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, name, project_dir, all_labels, user_url, data_url,
                 infile=None, columns=None, row_annotate=True,
                 col_annotate=True, text_annotate=True):
        self.name = name
        self.project_dir = Path(project_dir)
        self.row_annotate = row_annotate
        self.col_annotate = col_annotate
        self.text_annotate = text_annotate

        # Make sure the project directory exists
        self.project_dir.mkdir(parents=True, exist_ok=True)

        if labels is None or len(all_labels) == 0:
            raise TypeError("labels are not defined")

        # Deal with any labels that have been given
        self._labels = []
        # We check to see if we get given two labels of the same name
        self._labels_names = []
        for i in all_labels:
            if i.name in self._labels_names:
                raise KeyError(f"label name already given: {i.name}")
            self.add_label(i)
            self._labels_names.append(i.name)

        if len(self._labels) == 0:
            raise ValueError("no labels have been defined")

        if infile is not None and not isinstance(infile,
                                                 file_init.FileHandler):
            raise TypeError("infile must be of type: `{0}`".format(
                file_init.FileHandler.__class__.__name__
            ))
        self.infile = infile
        self.data_url = data_url
        self.users_url = user_url

        # Deal with any columns
        self._columns = []
        columns = columns or list()
        for i in columns:
            self.add_column(i)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing.
        """
        atts = [
            f"project_name='{self.name}'",
            f"project_dir='{self.project_dir}'",
            f"labels='{','.join([i.name for i in self.labels])}'",
            f"data_url='{self.data_url}'",
            f"users_url='{self.users_url}'"
        ]

        if self.infile is not None:
            atts.append(f"infile='{self.infile.name}'")

        if self.columns is not None:
            atts.append(
                f"columns='{','.join([c.name for c in self.columns])}'"
            )

        return "<{0}({1})>".format(
            self.__class__.__name__, ",".join(atts)
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def labels(self):
        """Get a copy of all the labels associated with the config file
        (`list` of `entity_tagger.labels.Label`)
        """
        return list(self._labels)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def columns(self):
        """Get a copy of all the columns associated with the config file
        (`list` of `entity_tagger.columns.Column`)
        """
        return list(self._columns)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def init_file(self):
        """Get the path to the initialised config file (`str`)
        """
        return os.path.join(self.project_dir, self.INIT_FILE)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_label(self, label):
        """Add a label to the config.

        Parameters
        ----------
        label : `entity_tagger.label.Label`
            The label object to add.
        """
        if not isinstance(label, labels.Label):
            raise TypeError("labels should be of type: `{0}`".format(
                labels.Label.__class__.__name_
            ))
        self._labels.append(label)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def add_column(self, column):
        """Add a column to the config.

        Parameters
        ----------
        column : `entity_tagger.columns.Column`
            A column object to add.
        """
        if not isinstance(column, columns.Column):
            raise TypeError("labels should be of type: `{0}`".format(
                labels.Label.__class__.__name_
            ))
        self._columns.append(column)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def write_config(self, path):
        """Write all the config options to a config file.

        Parameters
        ----------
        path : `str`
            The path to the output config file.

        Notes
        -----
        This writes a case sensitive config file.
        """
        cfg = self.build_configparser()
        print(cfg)
        with open(path, 'w') as conf:
            cfg.write(conf)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def build_configparser(self):
        """Build a config parser object with all the options in this config
        file.

        Returns
        -------
        cfg : `configparser.ConfigParser`
            The populated config parser object.
        """
        cfg = configparser.ConfigParser()
        cfg.optionxform = str
        cfg[self.PROJECT_SECTION] = {
            self.NAME_KEY: self.name,
            self.PATH_KEY: self.project_dir,
            self.ROW_ANNOTATE_KEY: self.row_annotate,
            self.COL_ANNOTATE_KEY: self.col_annotate,
            self.TEXT_ANNOTATE_KEY: self.text_annotate
        }
        cfg[self.DATABASE_SECTION] = {
            self.DATA_DB_KEY: self.data_url
        }
        self._build_infile(cfg)
        self._build_labels(cfg)
        self._build_columns(cfg)
        return cfg

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _build_infile(self, cfg):
        """Build the input file section for the configparser object.

        Parameters
        ----------
        cfg : `configparser.ConfigParser`
            The populated config parser object.
        """
        delimiter = self.infile.delimiter
        for m, s in [(r'\t', '\t'), (r'\s', " ")]:
            if delimiter == s:
                delimiter = m

        cfg[self.INFILE_SECTION] = {
            self.PATH_KEY: self.infile.name,
            self.ENCODING_KEY: self.infile.encoding,
            self.COMMENT_KEY: self.infile.comment,
            self.SKIPLINES_KEY: self.infile.skiplines,
            self.DELIMITER_KEY: delimiter
        }

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _build_labels(self, cfg):
        """Build the complex labels section for the configparser object.

        Parameters
        ----------
        cfg : `configparser.ConfigParser`
            The populated config parser object.
        """
        for i in self.labels:
            section_header = f"{self.COMPLEX_LABEL_SECTION}.{i.name}"
            cfg[section_header] = {
                self.LABEL_COLOR_KEY: i.colour,
                self.LABEL_COLOR_KEY: i.font_colour,
                self.LABEL_DESC_KEY: i.desc
            }

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _build_columns(self, cfg):
        """Build the complex columns section for the configparser object.

        Parameters
        ----------
        cfg : `configparser.ConfigParser`
            The populated config parser object.
        """
        keys = [self.COLUMN_DISPLAY_KEY, self.COLUMN_MAX_LEN_KEY,
                self.COLUMN_DISPLAY_LEN_KEY]
        bool_keys = [self.COLUMN_NULLABLE_KEY, self.COLUMN_INDEX_KEY,
                     self.COLUMN_ORDERABLE_KEY, self.COLUMN_SEARCHABLE_KEY,
                     self.COLUMN_VISIBLE_KEY, self.COLUMN_EDITABLE_KEY,
                     self.COLUMN_EXACT_QUERY_KEY]

        for i in self.columns:
            section_header = f"{self.COMPLEX_COLUMN_SECTION}.{i.name}"
            cfg[section_header] = dict([(k, getattr(i, k)) for k in keys])
            cfg[section_header][self.COLUMN_DTYPE_KEY] = i.dtype.__name__

            for k in bool_keys:
                cfg[section_header][k] = str(int(getattr(i, k)))


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class ConfigFile(Config):
    """Initialise a configuration object from a configuration file.

    Parameters
    ----------
    config_file : `str`
        The path to the configuration file.

    Raises
    ------
    ValueError
        If the configuration file does not have a .cnf, .ini or .cfg file
        extension.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, config_file):
        try:
            # Attempt to open the config file
            open(config_file).close()
        except FileNotFoundError as e:
            raise FileNotFoundError(
                "can't find configuration file"
            ) from e

        # Make sure the config file has the correct file extensions
        # TODO: Check permissions (readable only by the user)
        self._check_config_ext(config_file)

        self.config_parser = configparser.ConfigParser()
        self.config_parser.optionxform = str
        self.config_parser.read(config_file)

        # Make sure all the expected sections exist
        for i in self._EXPECTED_SECTIONS:
            try:
                self.config_parser[i]
            except KeyError as e:
                raise KeyError(f"config file must have section: {i}") from e

        users_url, data_url = self.parse_database()
        labels = self.parse_simple_labels()
        labels.extend(self.parse_complex_labels())

        # parse the columns
        cols = self.parse_complex_columns()

        name, path, row_annotate, col_annotate, text_annotate = \
            self.parse_project()
        # users_url, data_url = None, None
        super().__init__(
            name, path, labels, users_url, data_url,
            infile=self.parse_infile(), columns=cols,
            row_annotate=row_annotate, col_annotate=col_annotate,
            text_annotate=text_annotate
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse_project(self):
        """Parse the project from the config file.

        Returns
        -------
        project_path : `pathlib.Path`
            The path to the project directory as defined in the config file.
        """
        try:
            name = self.config_parser[self.PROJECT_SECTION][self.NAME_KEY]
        except KeyError as e:
            raise KeyError("project name not defined") from e

        try:
            project = self.config_parser[self.PROJECT_SECTION][self.PATH_KEY]
        except KeyError as e:
            raise KeyError("project path not defined") from e

        annotations = []
        for i in [self.ROW_ANNOTATE_KEY, self.COL_ANNOTATE_KEY,
                  self.TEXT_ANNOTATE_KEY]:
            try:
                annotations.append(
                    bool(int(self.config_parser[self.PROJECT_SECTION][i]))
                )
            except KeyError:
                annotations.append(True)

        return name, Path(project), *annotations

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse_simple_labels(self):
        """Parse any simple label fields from the config file.

        Returns
        -------
        project_path : `pathlib.Path`
            The path to the project directory as defined in the config file.
        """
        all_labels = []
        try:
            for k, v in self.config_parser[self.LABEL_SECTION].items():
                all_labels.append(labels.Label(k, desc=v))
        except KeyError:
            pass

        return all_labels

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse_complex_labels(self):
        """Parse any complex label sections from the config file.

        Returns
        -------
        complex_labels : `list` of `entity_tagger.labels.Label`
            Any complex labels, this will be an empty list of there are none.
        """
        all_labels = []
        for sh, sk in self.config_parser.items():
            label_reg = re.match(
                r'{0}\.(?P<NAME>\w+)$'.format(self.COMPLEX_LABEL_SECTION), sh
            )
            if label_reg:
                kwargs = dict(sk)
                all_labels.append(
                    labels.Label(label_reg.group('NAME'), **kwargs)
                )
        return all_labels

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse_complex_columns(self):
        """Parse any complex column sections from the config file.

        Returns
        -------
        complex_labels : `list` of `entity_tagger.columns.Column`
            Any complex columns, this will be an empty list of there are none.
        """
        cols = []
        for sh, sk in self.config_parser.items():
            col_reg = re.match(
                r'{0}\.(?P<NAME>\w+)$'.format(self.COMPLEX_COLUMN_SECTION), sh
            )
            if col_reg:
                kwargs = dict(sk)

                try:
                    dtype = str
                    if kwargs[self.COLUMN_DTYPE_KEY] == 'int':
                        dtype = int
                    elif kwargs[self.COLUMN_DTYPE_KEY] == 'float':
                        dtype = float
                    kwargs[self.COLUMN_DTYPE_KEY] = dtype
                except KeyError:
                    pass

                # Loop through all the integer keys to add them to the column
                for i in [
                        self.COLUMN_MAX_LEN_KEY, self.COLUMN_DISPLAY_LEN_KEY
                ]:
                    try:
                        kwargs[i] = int(kwargs[i])
                    except KeyError:
                        pass

                # Boolean keys/values
                for i in [
                        self.COLUMN_NULLABLE_KEY, self.COLUMN_INDEX_KEY,
                        self.COLUMN_ORDERABLE_KEY, self.COLUMN_SEARCHABLE_KEY,
                        self.COLUMN_VISIBLE_KEY, self.COLUMN_EDITABLE_KEY,
                        self.COLUMN_EXACT_QUERY_KEY
                ]:
                    try:
                        kwargs[i] = bool(int(kwargs[i]))
                    except KeyError:
                        pass

                cols.append(
                    columns.Column(col_reg.group('NAME'), **kwargs)
                )
        return cols

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse_infile(self):
        """Parse the infile section from the config file.

        Returns
        -------
        project_path : `entity_tagger.file_init.FileHandler`
            The path to the project directory as defined in the config file.
        """
        try:
            section = self.config_parser[self.INFILE_SECTION]
        except KeyError:
            # optional
            return None

        try:
            name = section[self.PATH_KEY]
        except KeyError as e:
            raise ValueError("the path must be defined for input files") from e

        # The non-csv arguments
        file_kwargs = {}
        for i in [self.DELIMITER_KEY, self.ENCODING_KEY, self.COMMENT_KEY,
                  self.SKIPLINES_KEY]:
            try:
                file_kwargs[i] = section[i]
            except KeyError:
                pass

        try:
            # if delimiter has been set we need to convert \t (2 characters)
            # into proper tabs
            for m, s in [(r'\\s', '\t'), (r'\\s', " ")]:
                if re.match(m, file_kwargs[self.DELIMITER_KEY]):
                    file_kwargs[self.DELIMITER_KEY] = s
                    break
        except KeyError:
            pass
        return file_init.FileHandler(name, **file_kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def parse_database(self):
        """Parse the database section from the config file.

        Returns
        -------
        users_url : sqlalchemy.engine.URL
            The url for the users database.
        data_url : sqlalchemy.engine.URL
            The url for the data database.

        Notes
        -----
        The database is an optional section. Users can use it to place a custom
        database locations. If it is not defined then a URL is created
        automatically.
        """
        try:
            section = self.config_parser[self.DATABASE_SECTION]
        except KeyError:
            # optional
            users_url = self._create_default_db_url(c.DEFAULT_USER_DB_NAME)
            data_url = self._create_default_db_url(
                self._create_default_db_name()
            )
            return users_url, data_url

        try:
            users_url = section[self.USERS_DB_KEY]
        except KeyError:
            users_url = self._create_default_db_url(c.DEFAULT_USER_DB_NAME)

        try:
            data_url = section[self.DATA_DB_KEY]
        except KeyError:
            data_url = self._create_default_db_url(
                self._create_default_db_name()
            )
        return users_url, data_url

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _create_default_db_url(self, name):
        """Create a default DB URL.

        Parameters
        ----------
        name : `str`
            The name of the database.

        Returns
        -------
        url : sqlalchemy.engine.URL
            The url for the default database with ``name``.

        Notes
        -----
        If the default back end is SQLite then .db file extension is added if
        it is not present.
        """
        db_backend = c.DEFAULT_DB_BACKEND
        if c.DEFAULT_DRIVER is not None:
            db_backend = "{db_backend}+{c.DEFAULT_DRIVER}"

        if c.DEFAULT_DB_BACKEND == 'sqlite':
            if not name.endswith('.db'):
                name = f"{name}.db"
            project_name, path, ra, ca, ta = self.parse_project()
            name = os.path.join(path, name)
        return URL(database=name, drivername=db_backend)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def _create_default_db_name(self):
        """Create a default database name from an input file name.
        """
        file_handler = self.parse_infile()
        name = re.sub(
            r'\W+', ' ', os.path.basename(
                file_handler.name.with_suffix('')
            ).lower()
        )
        name = re.sub(r'\s+', '_', name)
        return name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @classmethod
    def _check_config_ext(cls, config_file):
        """Ensure the config file extension is valid.

        Parameters
        ----------
        config_file : `str`
            The path to the configuration file.

        Raises
        ------
        ValueError
            If the configuration file does not have a .cnf, .ini or .cfg file
            extension
        """
        ext_ok = False
        for i in cls.CONFIG_FILE_EXT:
            if config_file.lower().endswith(i.lower()):
                ext_ok = True
                break

        if ext_ok is False:
            raise ValueError(
                "config file should have extensions: {0}".format(
                    ",".join(c.CONFIG_FILE_EXT)
                )
            )
