from flask import (
    current_app,
    render_template,
    jsonify,
    request,
    url_for
)
from flask.views import MethodView
import os
import pprint as pp


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    name = current_app.name
    pp.pprint(current_app.labels)
    return render_template(
        'index.html',
        title=name,
        init_root=url_for('root', _external=True),
        js_init=url_for('static', filename="js/init.js", _external=True),
        js_tb=url_for('static', filename="js/textbox.js", _external=True),
        labels=current_app.labels
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def data():
    """Endpoint to get the data definitions.

    Returns
    -------
    response : `flask.json`
        The data for the current data table data.

    Notes
    -----
    This also handles searching and data ordering, pagination etc...
    """
    query = current_app.query
    columns = current_app.columns
    # order_column = current_app.columns[request.args.get('draw')]

    column_order = _get_order_columns(request, columns)
    search_cols = _get_search_columns(request, columns)
    # pp.pprint(column_order)
    results = []
    total_records = query.count
    nmatches = total_records
    with query as q:
        if len(search_cols) == 0:
            for row in q.get_data(
                    offset=request.args.get('start'),
                    nrows=request.args.get('length'),
                    order=column_order
            ):
                dr = dict([(i.name, getattr(row, i.name)) for i in columns])
                dr['ATTRIBUTES'] = dict()
                results.append(dr)
        else:
            sql, nmatches = q.search_data(
                search_cols,
                offset=request.args.get('start'),
                nrows=request.args.get('length'),
                order=column_order
            )
            for row in sql:
                dr = dict([(i.name, getattr(row, i.name)) for i in columns])
                dr['ATTRIBUTES'] = dict()
                results.append(dr)
    # pp.pprint(request.args)
    # pp.pprint(search_cols)
    response = dict(
        draw=int(request.args.get('draw')),
        recordsTotal=total_records,
        recordsFiltered=nmatches,
        data=results,
    )
    return jsonify(response)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_search_columns(request, columns):
    """Attempt to locate any search columns from the request.

    Parameters
    ----------
    request : `flask.Request`
        The flask request. This is expected to be a GET request.
    columns : `list` of `entity_tagger.columns.Column`
        The column objects passed to the App.

    Returns
    -------
    search_cols : `list` of `(col_name, search_term, use_regex)`
        The columns that we want to search.
    """
    bool_map = dict(true=True, false=False)

    # TODO: cast the search term to the correct type for column?
    global_search_term = request.args.get('search[value]')

    # Otherwise we check all the columns for column specific searches
    idx = 0
    search_cols = []
    while True:
        try:
            search_term = request.args[f'columns[{idx}][search][value]']
            searchable = bool_map[request.args[f'columns[{idx}][searchable]']]
            regex = bool_map[request.args[f'columns[{idx}][search][regex]']]
            if searchable is True:
                if search_term != '':
                    search_cols.append(
                        (columns[idx].name, search_term, regex)
                    )
                if global_search_term != '':
                    search_cols.append(
                        (columns[idx].name, global_search_term, regex)
                    )
        except KeyError:
            return search_cols
        idx += 1


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _get_order_columns(request, columns):
    """Get the column data order direction from the request.

    Parameters
    ----------
    request : `flask.Request`
        The flask request. This is expected to be a GET request.
    columns : `list` of `entity_tagger.columns.Column`
        The column objects passed to the App.

    Returns
    -------
    ordering : `list` of `(col_name, search_term, direction)`
        The columns that we want to order on and their direction
        (``asc``, ``desc``).
    """
    ordering = []
    idx = 0
    while True:
        try:
            col_no = int(request.args[f'order[{idx}][column]'])
            col_dir = request.args[f'order[{idx}][dir]']
            ordering.append((columns[col_no].name, col_dir))
        except KeyError:
            break
        idx += 1
    return ordering


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def setup():
    """Endpoint to get the column, labels and annotation level definitions.
    """
    return_cols = []
    for i in current_app.columns:
        c = i.to_dict()
        c['dtype'] = c['dtype'].__name__
        return_cols.append(c)

    return_labels = []
    for i in current_app.labels:
        lab = i.to_dict()
        return_labels.append(lab)

    return jsonify(dict(
        columns=return_cols, labels=return_labels,
        row_annotate=current_app.row_annotate,
        col_annotate=current_app.col_annotate,
        text_annotate=current_app.text_annotate
    ))
