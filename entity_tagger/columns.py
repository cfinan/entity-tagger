"""Column definition classes.
"""
from sqlalchemy import Column as SqlAlChemyColumn, Integer, String, Float


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Column(object):
    """A representation of a column.

    Parameters
    ----------
    name : `str`
        The column name.
    dtype : `str` or `float` or `int` or `bool`
        The column data type. These are types not values.
    nullable : `bool`, optional, default: `False`
        Can the column contain ``NoneType`` values.
    max_len : `int`, optional, default: `0`
        If the dtype is a string then what is the max value.
    index : `bool`, optional, default: `False`
        Should the column be indexed.
    display_name : `str`, optional, default: `NoneType`
        The name of the column in the web display. If not provided then the
        ``name`` is used.
    display_len : `bool`, optional, default: `True`
        The display length for the text in the column when rendered by
        DataTables.
    orderable : `bool`, optional, default: `True`
        Can the column be sorted in the DataTable GUI.
    searchable : `bool`, optional, default: `True`
        Can the column be searched in the DataTable GUI.
    visible : `bool`, optional, default: `True`
        Is the column visible in the DataTable GUI.
    editable : `bool`, optional, default: `True`
        Is the column editable in the DataTable GUI.
    internal_delimiter : `str`, optional, default: `NoneType`
        Does the column have any strings that are internally delimited. If so,
        supply the delimiter here.``NoneType`` means no.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, name, dtype=str, nullable=False, max_len=0, index=False,
                 display_name=None, display_len=0, orderable=True,
                 searchable=True, visible=True, editable=True,
                 exact_query=True, internal_delimiter=None):
        self.name = name
        self.dtype = dtype
        self.nullable = nullable
        self.max_len = max_len
        self.index = index
        self._display_name = display_name
        self.display_len = display_len
        self.orderable = orderable
        self.searchable = searchable
        self.visible = visible
        self.editable = editable
        self.exact_query = exact_query
        self.internal_delimiter = internal_delimiter

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing.
        """
        atts = [
            f"name='{self.name}'",
            f"dtype='{self.dtype}'",
            f"nullable='{self.nullable}'",
            f"max_len='{self.max_len}'",
            f"index='{self.index}'",
            f"display_length='{self.display_len}'",
            f"orderable='{self.orderable}'",
            f"searchable='{self.searchable}'",
            f"visible='{self.visible}'",
            f"editable='{self.editable}'",
            f"exact_query='{self.exact_query}'",
            f"internal_delimiter='{self.internal_delimiter}'",
        ]

        return "<{0}({1})>".format(
            self.__class__.__name__, ",".join(atts)
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def display_name(self):
        """Get the display name (`str`)
        """
        if self._display_name is None:
            return self.name
        return self._display_name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @display_name.setter
    def display_name(self, display_name):
        """Set the display name (`str`)
        """
        self._display_name = display_name

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def sqlalchemy(self):
        """Get the SQL Alchemy column definition for the column.

        Returns
        -------
        sqlalchemy_column : `sqlalchemy.Column`
            The SQLAlchemy column for the column definition.
        """
        dtype = None
        test = self.dtype("1")
        if isinstance(test, str):
            max_len = self.max_len
            if self.max_len == 0:
                max_len = 100
            dtype = String(max_len)
        elif isinstance(test, int):
            dtype = Integer
        elif isinstance(test, float):
            dtype = Float
        else:
            raise TypeError(f"unknown data type: {self.dtype}")

        args = [self.name, dtype]
        kwargs = dict(nullable=self.nullable, index=self.index)
        return SqlAlChemyColumn(*args, **kwargs)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def to_dict(self):
        """Convert the object attributes to a dictionary.

        Returns
        -------
        as_dict : `dict`
            A dictionary of the object attributes.
        """
        return dict(
            name=self.name,
            display_name=self.display_name,
            dtype=self.dtype,
            nullable=self.nullable,
            max_len=self.max_len,
            index=self.index,
            display_len=self.display_len,
            orderable=self.orderable,
            searchable=self.searchable,
            visible=self.visible,
            editable=self.editable,
            exact_query=self.exact_query,
            internal_delimiter=self.internal_delimiter
        )
