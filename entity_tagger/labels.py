"""Classes representing labels
"""
from itertools import cycle
from matplotlib import colors


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class Label(object):
    """A label container class.

    Parameters
    ----------
    name : `str`
        The label name, must be unique (i.e. not used in any other label
        object).
    desc : `str`, optional, default: `NoneType`
        A label description.
    colour : `str`, optional, default: `NoneType`
        Label colour, if not defined then the default colours are cycled
        through.

    Raises
    ------
    KeyError
        If the label name has been used previously.
    """
    DEFAULT_LABEL_COLOURS = [
        "red", "blue", "green", "orange"
    ]
    """The default label colours (`list` of `str`)
    """
    _COLOR_CYCLER = cycle(DEFAULT_LABEL_COLOURS)
    """A cycler generator for the label colours (if the user does not define)
    (`set`)
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, name, desc=None, colour=None, font_colour='black'):
        name = name.strip()

        # Make sure the name is defined correctly (i.e. not empty)
        if name is None or len(name) == 0:
            raise ValueError("name not defined")

        # Name is good so we store and add to the class lookup
        self.name = name

        # clean any white space
        self.desc = desc.strip()

        # If the colour is not defined by the user then cycle through the
        # defaults
        self.colour = colour or next(self._COLOR_CYCLER)
        self.display_colour = self.add_alpha(self.colour, 0.5)
        self.font_colour = font_colour

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __repr__(self):
        """Pretty printing
        """
        return (
            f"<{self.__class__.__name__}(name={self.name}), desc={self.desc}, "
            f" colour={self.colour}, display_colour={self.display_colour}, "
            f"font_colour={self.font_colour})>"
        )

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def add_alpha(colour, alpha):
        """Utility function for adding an alpha to a colour. Added as I keep on
        forgetting how to do this!

        Parameters
        ----------
        color : `color`
            An object that can be interpreted  as a colour by `matplotlib`
        alpha : `float`
            The amount of alpha to add to the colour. 1 is no alpha and 0 is full
            alpha.

        Returns
        -------
        color_with_alpha : `color`
            The colour with the alpha added.
        """
        alpha_col = colors.to_rgba(colour, alpha=alpha)
        return colors.to_hex(alpha_col, keep_alpha=True)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def to_dict(self):
        """Convert the object attributes to a dictionary.

        Returns
        -------
        as_dict : `dict`
            A dictionary of the object attributes.
        """
        return dict(name=self.name, desc=self.desc, colour=self.colour)
