"""Functions and classes for database interaction.
"""
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

try:
    from sqlalchemy.engine import make_url
except ImportError:
    from sqlalchemy.engine.url import make_url

from sqlalchemy.engine.url import URL
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer
import os

Base = declarative_base()
"""Use to create the dynamic table
 (`sqlalchemy.ext.declarative import declarative_base.Base`)
"""


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class SessionHandler(object):
    """Handle multiple SQLAlchemy sessions that point to multiple databases.

    Parameters
    ----------
    *urls
        One or more SQLAlchemy URLs, These can either be strings or URL
        objects. You can also pass a tuple of (URL, name), where name is the
        identifier for the session in the methods of this class. If the name
        is not provided then the name is taken from the database name. If the
        database is SQLite, then the database name will be the basename of the
        file. Database names must be unique.
    """
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, *urls):
        if len(urls) == 0:
            raise ValueError("no URLs provided")

        self._urls = dict()
        self._engines = dict()
        self._session_makers = dict()
        self._sessions = dict()

        for i in urls:
            if isinstance(i, tuple):
                url_obj = make_url(i[0])
                dbname = i[1]
            else:
                url_obj = self.make_url_obj(i)
                dbname = self.get_name_from_url(url_obj)

            if dbname in self._urls:
                raise KeyError(
                    "database name already exists (must be unique): {dbname}"
                )

            self._urls[dbname] = url_obj

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def dbnames(self):
        """Get all the database names (in order given) (`list` of `str`).

        Returns
        -------
        names : `list` of `str`
            The database names.
        """
        return list(self._urls.keys())

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def make_url_obj(url):
        """Generate an SQLAlchemy URL object.

        Parameters
        ----------
        url : `str` or `sqlalchemy.engine.url.URL`
            A URL to process, if already a URL then this is returned, if a
            string then this is parsed into a URL object.

        Returns
        -------
        url_obj : `sqlalchemy.engine.url.URL`
            An SQLAlchemy URL object.

        Raises
        ------
        TypeError
            If the URL is not a string or `sqlalchemy.engine.url.URL`.
        """
        if isinstance(url, str):
            return make_url(url)
        elif isinstance(url, URL):
            return url
        raise TypeError("unknown input: {url.__class__.__name__}")

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @staticmethod
    def get_name_from_url(url):
        """Generate a database name from an SQLAlchemy URL.

        Parameters
        ----------
        url : `sqlalchemy.engine.url.URL`
            An SQLAlchemy URL object.

        Returns
        -------
        name : `str`
            The database name.
        """
        if url.drivername == 'sqlite':
            return os.path.basename(url.database)
        return url.database

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_session(self, name):
        """Get a database session.

        Parameters
        ----------
        name : `str`
            The database name to for the session.

        Returns
        -------
        session : `sqlalchemy.Session`
            The database session.

        Raises
        ------
        KeyError
            If the session does not exist.
        """
        try:
            return self._sessions[name]
        except KeyError as e:
            raise KeyError(
                f"unknown session: {name}, have you opened the SessionHandler"
            ) from e

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self, dbnames=None):
        """Open the database.

        Parameters
        ----------
        dbname : `list` of `str`, optional, default: `NoneType`
            The database names to open, if not provided then all databases will
            be opened. If a database is already open then this will be silently
            ignored.
        """
        if dbnames is None or len(dbnames) == 0:
            dbnames = self.dbnames

        for i in dbnames:
            try:
                self._engines[i]
            except KeyError:
                self._engines[i] = create_engine(self._urls[i])

            try:
                self._session_makers[i]
            except KeyError:
                self._session_makers[i] = sessionmaker(
                    autocommit=False,
                    autoflush=False,
                    bind=self._engines[i]
                )

            try:
                self._sessions[i]
            except KeyError:
                self._sessions[i] = scoped_session(self._session_makers[i])

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self, dbnames=None):
        """Open the database.

        Parameters
        ----------
        dbname : `list` of `str`, optional, default: `NoneType`
            The database names to open, if not provided then all databases will
            be opened. If a database is already open then this will be silently
            ignored.
        """
        if dbnames is None or len(dbnames) == 0:
            dbnames = self.dbnames

        for i in dbnames:
            try:
                self._sessions[i].close()
                self._sessions[i].remove()
                del self._sessions[i]
            except KeyError:
                pass


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_engine(url):
    """Get an SQLAlchemy engine connected to the URL.

    Parameters
    ----------
    url : `str`
        The SQLAlchemy URL connection string.

    Returns
    -------
    engine : `sqlalchemy.engine.base.Engine`
        The SQLAlchemy engine.
    """
    return create_engine(
        url, connect_args={"check_same_thread": False}
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_local_sessionmaker(engine):
    """Get a sessionmaker bound to the engine.

    Parameters
    ----------
    engine : `sqlalchemy.engine.base.Engine`
        The SQLAlchemy engine.
    """
    return sessionmaker(
        autocommit=False, autoflush=False, bind=engine
    )


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_table(class_name, table_name, columns):
    """Dynamically create a table.

    Parameters
    ----------
    class_name : `str`
        The class name for the ORM class.
    table_name : `str`
        The table name for the database table represented by the ORM class.
    columns : `list` of `sqlalchemy.Column`
        The columns that will be in the table. Importantly, the columns must
        have a name attribute.

    Returns
    -------
    table : `entity_tagger.table.<class_name>`
        The table ORM class that represents ``table_name``.
    """
    attr_dict = {'__tablename__': table_name}
    attr_dict['row_pk'] = Column(Integer, autoincrement=True, primary_key=True)
    for i in columns:
        attr_dict[i.name] = i
    return type(class_name, (Base,), attr_dict)
