"""Initialisation functions when operating in file mode.
"""
from entity_tagger import columns
from pathlib import Path
from tqdm import tqdm
import hashlib
import binascii
import gzip
import csv
import re
import pprint as pp


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class FileHandler(object):
    """A container class for the options of an input file.

    Parameters
    ----------
    infile : `str`
        The input file name.
    encoding : `str`, optional, default: 'utf8'
        The encoding of the file.
    comment : `str`, optional, default: `#`
        Comment characters.
    skiplines : `int`, optional, default: `0`
        Skip this many lines before reading the file.
    delimiter : `str`, optional, default: `\t`
        The default delimiter for the file.
    **csv_kwargs
        Any other keyword arguments passed to csv.
    """
    DEFAULT_DELIMITER = "\t"
    """The default input file delimiter (`str`)
    """
    DEFAULT_ENCODING = "UTF8"
    """The default input file encoding (`str`)
    """
    DEFAULT_COMMENT = "#"
    """The default input file comment line (`str`)
    """
    DEFAULT_SKIPLINES = 0
    """The default number of lines to skip (`str`)
    """
    FLOAT_REGEX = re.compile(r'^-?(?:0|[1-9]\d*)(?:\.\d+)(?:[eE][+\-]?\d+)?$')
    """A float regex (this will not match ints) (`re.Pattern`)
    """
    INT_REGEX = re.compile(r'^-?(?:0|[1-9]\d*)(?:[eE][+\-]?\d+)?$')
    """An integer regex (`re.Pattern`)
    """

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, infile, encoding=DEFAULT_ENCODING,
                 comment=DEFAULT_COMMENT, skiplines=DEFAULT_SKIPLINES,
                 delimiter=DEFAULT_DELIMITER, **csv_kwargs):
        self.name = Path(infile)

        if self.name.exists() is False:
            raise TypeError("input file does not exist")

        if self.name.is_file() is False:
            raise IOError("input is not a file")

        self.encoding = encoding
        self.comment = comment
        self.skiplines = skiplines
        self.delimiter = delimiter
        self.csv_kwargs = csv_kwargs

        # Will hold the number of rows in the file, this will be lazy obtained
        self._nrows = None

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def nrows(self):
        """Get the number of rows in the file, excluding header, comments
        etc... (`int`)
        """
        if self._nrows is None:
            # Read will count the rows
            for i in self.read():
                pass
        return self._nrows

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_md5(self):
        """Get an MD5 hash of the file.

        Returns
        -------
        md5hash : `str`
            The 32 character MD5 hash of the file.
        """
        with open(self.name, "rb") as f:
            file_hash = hashlib.md5()
            while chunk := f.read(8192):
                file_hash.update(chunk)
        return file_hash.hexdigest()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def is_gzip(self):
        """Test of the file is gzipped.

        Returns
        -------
        is_gzipped : `bool`
            ``True`` if the file is gzipped, ``False`` if not.
        """
        with open(self.name, 'rb') as test:
            # We read the first two bytes and we will look at their values
            # to see if they are the gzip characters
            testchr = test.read(2)
            is_gzipped = False

            # Test for the gzipped characters
            if binascii.b2a_hex(testchr).decode() == "1f8b":
                # If it is gziped, then close it and reopen as a gzipped file
                is_gzipped = True

        return is_gzipped

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_fobj(self):
        """Get a file object. Agnostic to gzipped.

        Returns
        -------
        fobj : `_io.TextIOWrapper` or ``
            A file object open in text mode.
        """
        if self.is_gzip() is False:
            return open(self.name, 'rt', encoding=self.encoding)
        else:
            return gzip.open(self.name, 'rt', encoding=self.encoding)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def read(self):
        """Yield rows from the input file. This sorts comment/skiplines.

        Yields
        ------
        row : `list` of `str`
            Valid rows from the file, the first row is always the header row.
        """
        nlines = 0
        # Will hold the count of rows
        count = 0
        with self.get_fobj() as infile:
            # sort the skip lines
            while nlines < self.skiplines:
                try:
                    next(infile)
                except StopIteration:
                    raise IndexError("skipping too many rows for file")
            reader = csv.reader(infile, delimiter=self.delimiter,
                                **self.csv_kwargs)

            # Sort the header
            header = [self.comment]
            while header[0].startswith(self.comment):
                header = next(reader)
                header = [i.strip() for i in header]

            yield header

            for row in reader:
                # count the rows
                count += 1
                row = [i.strip() for i in row]
                if row[0].startswith(self.comment):
                    continue
                yield row
            self._nrows = count

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_dtypes(self, verbose=False):
        """Scan the file and check the datatypes of each of the columns.

        Parameters
        ----------
        verbose : `bool`, optional, default: `False`
            Report progress through the file.

        Returns
        -------
        columns : `list` of `columns.Column`
            Columns objects that define the data types.

        Notes
        -----
        Currently this only distinguishes between ``float``, ``int`` and
        ``str``. Although, ``bool`` may be possible in future.
        """
        reader = self.read()
        header = next(reader)
        # Initialise to floats as we will disprove this
        cols = [columns.Column(i, dtype=float, nullable=False) for i in header]
        for row in tqdm(reader, disable=not verbose, unit=" rows",
                        desc="[info] extract dtypes", leave=False):
            for idx, col in enumerate(cols):
                cell = row[idx]
                if cell is None or cell == '':
                    col.nullable = True
                    continue

                # Take the length anyway, just in case is is move to
                # string later
                col.max_len = max(len(cell), col.max_len)
                if col.dtype == str:
                    # Already at the lowest level
                    continue

                # Not a float, so we will call it in int
                if not self.FLOAT_REGEX.match(cell):
                    col.dtype = int
                else:
                    continue

                # Not a int, so we will call it a string
                if not self.INT_REGEX.match(cell):
                    col.dtype = str
        return cols
