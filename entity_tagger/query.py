"""The query handling.
"""
from sqlalchemy import asc, desc, or_


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
class QueryInterface(object):
    """
    """
    _ORDER_MAP = dict(asc=asc, desc=desc)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __init__(self, handler, local_table):
        self.handler = handler
        self.local_table = local_table
        try:
            self.handler.open()
            self._count = self.get_total_count()
        finally:
            self.handler.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __enter__(self):
        self.open()
        return self

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def __exit__(self, *args):
        self.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def open(self, dbnames=None):
        """Open the database.

        Parameters
        ----------
        dbname : `list` of `str`, optional, default: `NoneType`
            The database names to open, if not provided then all databases will
            be opened. If a database is already open then this will be silently
            ignored.
        """
        self.handler.open()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def close(self, dbnames=None):
        """Open the database.

        Parameters
        ----------
        dbname : `list` of `str`, optional, default: `NoneType`
            The database names to open, if not provided then all databases will
            be opened. If a database is already open then this will be silently
            ignored.
        """
        self.handler.close()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    @property
    def count(self):
        return self._count

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_total_count(self):
        return self.handler.get_session('data').query(self.local_table).count()

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def get_data(self, offset=0, nrows=50, order=None):
        q = self.handler.get_session('data')\
                        .query(self.local_table)

        for col_name, direction in order:
            q = q.order_by(
                self._ORDER_MAP[direction](
                    getattr(self.local_table, col_name)
                )
            )

        return q.offset(offset).limit(nrows)

    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    def search_data(self, search_cols, offset=0, nrows=50, order=None):
        q = self.handler.get_session('data')\
                        .query(self.local_table)

        filters = []
        for col_name, search_val, like_search in search_cols:
            col = getattr(
                        self.local_table, col_name
                    )
            if like_search is False:
                filters.append(col == search_val)

                # q = q.filter(
                #     getattr(
                #         self.local_table, col_name
                #     ) == search_val
                # )
            else:
                filters.append(col.like(f"%{search_val}%"))
                # q = q.filter(col.like(f"%{search_val}%"))
        print(or_(*filters))
        q = q.filter(or_(*filters))
        for col_name, direction in order:
            q = q.order_by(
                self._ORDER_MAP[direction](
                    getattr(self.local_table, col_name)
                )
            )
        nmatches = q.count()
        return q.offset(offset).limit(nrows), nmatches
